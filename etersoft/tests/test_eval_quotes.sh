#!/bin/sh

# see /usr/bin/gear-hsh for example correct using

func()
{
	echo $1
	echo $2
	echo $3
}

E1='"a b"'
E2="\"c d\""
func $E1 $E2
echo
eval func $E1 $E2
echo N
eval echo $E1 $E2
eval /bin/echo $E1 $E2


VAR="Один два"
LIST="'$VAR' и ещё"
func $LIST
