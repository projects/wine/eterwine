#!/bin/sh

WINEHQ=git://source.winehq.org/git/wine.git

# parent dir is not missed in pure
cd ../..

# Updates all our branches from winehq
if [ "$1" != "-l" ] ; then
	git checkout pure && git pull --tags $WINEHQ master || exit 1
fi

echo "All done correctly"
