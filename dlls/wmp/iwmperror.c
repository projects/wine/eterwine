/*
 * IWMPError interface functions
 *
 * Copyright (C) 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"

/**********************************************************
 * IWMPError methods implementation
 */

#define ERROR_THIS(iface) DEFINE_THIS(Player, WMPError, iface)

static HRESULT WINAPI WMPError_QueryInterface(
        IWMPError* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = ERROR_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI WMPError_AddRef(
        IWMPError* iface)
{
    Player *This = ERROR_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI WMPError_Release(
        IWMPError* iface)
{
    Player *This = ERROR_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IDispatch methods ***/
static HRESULT WINAPI WMPError_GetTypeInfoCount(
        IWMPError* iface,
        UINT *pctinfo)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPError_GetTypeInfo(
        IWMPError* iface,
        UINT iTInfo,
        LCID lcid,
        ITypeInfo **ppTInfo)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPError_GetIDsOfNames(
        IWMPError* iface,
        REFIID riid,
        LPOLESTR *rgszNames,
        UINT cNames,
        LCID lcid,
        DISPID *rgDispId)
{
    Player *This = ERROR_THIS(iface);
    TRACE("\n");
    return IDispatchEx_GetIDsOfNames(DISPATCHEX(This), riid, rgszNames, cNames, lcid, rgDispId);
}

static HRESULT WINAPI WMPError_Invoke(
        IWMPError* iface,
        DISPID dispIdMember,
        REFIID riid,
        LCID lcid,
        WORD wFlags,
        DISPPARAMS *pDispParams,
        VARIANT *pVarResult,
        EXCEPINFO *pExcepInfo,
        UINT *puArgErr)
{
    Player *This = ERROR_THIS(iface);
    TRACE("\n");
    return IDispatchEx_Invoke(DISPATCHEX(This), dispIdMember, riid, lcid, wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr);
}

    /*** IWMPError methods ***/
static HRESULT WINAPI WMPError_clearErrorQueue(
        IWMPError* iface)
{
    TRACE("\n");
    return S_OK;
}

static HRESULT WINAPI WMPError_get_errorCount(
        IWMPError* iface,
        long *plNumErrors)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPError_get_item(
        IWMPError* iface,
        long dwIndex,
        IWMPErrorItem **ppErrorItem)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI WMPError_webHelp(
        IWMPError* iface)
{
    TRACE("is not implemented\n");
    return E_NOTIMPL;
}

const IWMPErrorVtbl WMPErrorVtbl =
{
    WMPError_QueryInterface,
    WMPError_AddRef,
    WMPError_Release,
    WMPError_GetTypeInfoCount,
    WMPError_GetTypeInfo,
    WMPError_GetIDsOfNames,
    WMPError_Invoke,
    WMPError_clearErrorQueue,
    WMPError_get_errorCount,
    WMPError_get_item,
    WMPError_webHelp
};

#undef ERROR_THIS

void Player_WMPError_Init(Player *This)
{
    This->lpWMPErrorVtbl = &WMPErrorVtbl;
}
