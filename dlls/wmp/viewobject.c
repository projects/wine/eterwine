/*
 * Copyright 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"

/**********************************************************
 * IViewObject methods implementation
 */

#define VIEWOBJ_THIS(iface) DEFINE_THIS(Player, ViewObject, iface)

static HRESULT WINAPI ViewObject_QueryInterface(
        IViewObject* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = VIEWOBJ_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI ViewObject_AddRef(
        IViewObject* iface)
{
    Player *This = VIEWOBJ_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI ViewObject_Release(
        IViewObject* iface)
{
    Player *This = VIEWOBJ_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IViewObject methods ***/
static HRESULT WINAPI ViewObject_Draw(
        IViewObject* iface,
        DWORD dwDrawAspect,
        LONG lindex,
        void *pvAspect,
        DVTARGETDEVICE *ptd,
        HDC hdcTargetDev,
        HDC hdcDraw,
        LPCRECTL lprcBounds,
        LPCRECTL lprcWBounds,
        BOOL (STDMETHODCALLTYPE *pfnContinue)(ULONG_PTR dwContinue),
        ULONG_PTR dwContinue)
{
//     WCHAR lpString[] = {'T','e','s','t',0};

    FIXME("is not implemented but return S_OK\n");

//     TextOutW(hdcDraw, lprcBounds->left+10, lprcBounds->top+10, lpString, 4);
    return S_OK;
}

static HRESULT WINAPI ViewObject_GetColorSet(
        IViewObject* iface,
        DWORD dwDrawAspect,
        LONG lindex,
        void *pvAspect,
        DVTARGETDEVICE *ptd,
        HDC hicTargetDev,
        LOGPALETTE **ppColorSet)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI ViewObject_Freeze(
        IViewObject* iface,
        DWORD dwDrawAspect,
        LONG lindex,
        void *pvAspect,
        DWORD *pdwFreeze)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI ViewObject_Unfreeze(
        IViewObject* iface,
        DWORD dwFreeze)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI ViewObject_SetAdvise(
        IViewObject* iface,
        DWORD aspects,
        DWORD advf,
        IAdviseSink *pAdvSink)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}


static HRESULT WINAPI ViewObject_GetAdvise(
        IViewObject* iface,
        DWORD *pAspects,
        DWORD *pAdvf,
        IAdviseSink **ppAdvSink)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static const IViewObjectVtbl ViewObjectVtbl =
{
    ViewObject_QueryInterface,
    ViewObject_AddRef,
    ViewObject_Release,
    ViewObject_Draw,
    ViewObject_GetColorSet,
    ViewObject_Freeze,
    ViewObject_Unfreeze,
    ViewObject_SetAdvise,
    ViewObject_GetAdvise
};

#undef VIEWOBJ_THIS

/**********************************************************
 * IViewObjectEx methods implementation
 */

#define VIEWOBJEX_THIS(iface) DEFINE_THIS(Player, ViewObjectEx, iface)

static HRESULT WINAPI ViewObjectEx_QueryInterface(
             IViewObjectEx* iface,
             REFIID riid,
             void **ppvObject)
{
    Player *This = VIEWOBJEX_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI ViewObjectEx_AddRef(
           IViewObjectEx* iface)
{
    Player *This = VIEWOBJEX_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI ViewObjectEx_Release(
           IViewObjectEx* iface)
{
    Player *This = VIEWOBJEX_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IViewObject methods ***/
static HRESULT WINAPI ViewObjectEx_Draw(
             IViewObjectEx* iface,
             DWORD dwDrawAspect,
             LONG lindex,
             void *pvAspect,
             DVTARGETDEVICE *ptd,
             HDC hdcTargetDev,
             HDC hdcDraw,
             LPCRECTL lprcBounds,
             LPCRECTL lprcWBounds,
             BOOL (STDMETHODCALLTYPE *pfnContinue)(ULONG_PTR dwContinue),
                   ULONG_PTR dwContinue)
{
    Player *This = VIEWOBJEX_THIS(iface);
    return ViewObject_Draw(VIEWOBJ(This), dwDrawAspect, lindex, pvAspect, ptd, hdcTargetDev, hdcDraw, lprcBounds, lprcWBounds, 0, dwContinue);
}

static HRESULT WINAPI ViewObjectEx_GetColorSet(
             IViewObjectEx* iface,
             DWORD dwDrawAspect,
             LONG lindex,
             void *pvAspect,
             DVTARGETDEVICE *ptd,
             HDC hicTargetDev,
             LOGPALETTE **ppColorSet)
{
    Player *This = VIEWOBJEX_THIS(iface);
    return ViewObject_GetColorSet(VIEWOBJ(This), dwDrawAspect, lindex, pvAspect, ptd, hicTargetDev, ppColorSet);
}

static HRESULT WINAPI ViewObjectEx_Freeze(
             IViewObjectEx* iface,
             DWORD dwDrawAspect,
             LONG lindex,
             void *pvAspect,
             DWORD *pdwFreeze)
{
    Player *This = VIEWOBJEX_THIS(iface);
    return ViewObject_Freeze(VIEWOBJ(This), dwDrawAspect, lindex, pvAspect, pdwFreeze);
}

static HRESULT WINAPI ViewObjectEx_Unfreeze(
             IViewObjectEx* iface,
             DWORD dwFreeze)
{
    Player *This = VIEWOBJEX_THIS(iface);
    return ViewObject_Unfreeze(VIEWOBJ(This), dwFreeze);
}

static HRESULT WINAPI ViewObjectEx_SetAdvise(
             IViewObjectEx* iface,
             DWORD aspects,
             DWORD advf,
             IAdviseSink *pAdvSink)
{
    Player *This = VIEWOBJEX_THIS(iface);
    return ViewObject_SetAdvise(VIEWOBJ(This), aspects, advf, pAdvSink);
}

static HRESULT WINAPI ViewObjectEx_GetAdvise(
             IViewObjectEx* iface,
             DWORD *pAspects,
             DWORD *pAdvf,
             IAdviseSink **ppAdvSink)
{
    Player *This = VIEWOBJEX_THIS(iface);
    return ViewObject_GetAdvise(VIEWOBJ(This), pAspects, pAdvf, ppAdvSink);
}

    /*** IViewObject2 methods ***/
static HRESULT WINAPI ViewObjectEx_GetExtent(
             IViewObjectEx* iface,
             DWORD dwDrawAspect,
             LONG lindex,
             DVTARGETDEVICE *ptd,
             LPSIZEL lpsizel)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

    /*** IViewObjectEx methods ***/
static HRESULT WINAPI ViewObjectEx_GetRect(
             IViewObjectEx* iface,
             DWORD dwAspect,
             LPRECTL pRect)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI ViewObjectEx_GetViewStatus(
             IViewObjectEx* iface,
             DWORD *pdwStatus)
{
    FIXME("return VIEWSTATUS_SOLIDBKGND\n");
    *pdwStatus = VIEWSTATUS_SOLIDBKGND;
    return S_OK;
}

static HRESULT WINAPI ViewObjectEx_QueryHitPoint(
             IViewObjectEx* iface,
             DWORD dwAspect,
             LPCRECT pRectBounds,
             POINT ptlLoc,
             LONG lCloseHint,
             DWORD *pHitResult)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI ViewObjectEx_QueryHitRect(
             IViewObjectEx* iface,
             DWORD dwAspect,
             LPCRECT pRectBounds,
             LPCRECT pRectLoc,
             LONG lCloseHint,
             DWORD *pHitResult)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI ViewObjectEx_GetNaturalExtent(
             IViewObjectEx* iface,
             DWORD dwAspect,
             LONG lindex,
             DVTARGETDEVICE *ptd,
             HDC hicTargetDev,
             DVEXTENTINFO *pExtentInfo,
             LPSIZEL pSizel)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static const IViewObjectExVtbl ViewObjectExVtbl =
{
    ViewObjectEx_QueryInterface,
    ViewObjectEx_AddRef,
    ViewObjectEx_Release,
    ViewObjectEx_Draw,
    ViewObjectEx_GetColorSet,
    ViewObjectEx_Freeze,
    ViewObjectEx_Unfreeze,
    ViewObjectEx_SetAdvise,
    ViewObjectEx_GetAdvise,
    ViewObjectEx_GetExtent,
    ViewObjectEx_GetRect,
    ViewObjectEx_GetViewStatus,
    ViewObjectEx_QueryHitPoint,
    ViewObjectEx_QueryHitRect,
    ViewObjectEx_GetNaturalExtent
};

#undef VIEWOBJEX_THIS

void Player_ViewObj_Init(Player *This)
{
    This->lpViewObjectVtbl = &ViewObjectVtbl;
    This->lpViewObjectExVtbl = &ViewObjectExVtbl;
}
