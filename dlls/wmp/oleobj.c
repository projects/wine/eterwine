/*
 * Copyright 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"
#include <shellapi.h>
#include <urlmon.h>

WCHAR eterbug1978_url[MAX_PATH] = {0};
WCHAR eterbug1978_baseurl[MAX_PATH] = {0};

/**********************************************************
 * IOleObject implementation
 */
#define OLEOBJ_THIS(iface) DEFINE_THIS(Player, OleObject, iface)

DEFINE_GUID(IID_IPersistMemory, 0xbd1ae5e0, 0xa6ae, 0x11ce, 0xbd,0x37, 0x50,0x42,0x00,0xc1,0x00,0x00);

static HRESULT WINAPI OleObject_QueryInterface(IOleObject *iface, REFIID riid, void **ppvObject)
{
    Player *This = OLEOBJ_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI OleObject_AddRef(IOleObject *iface)
{
    Player *This = OLEOBJ_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI OleObject_Release(IOleObject *iface)
{
    Player *This = OLEOBJ_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

static HRESULT WINAPI OleObject_SetClientSite(IOleObject *iface, IOleClientSite *pClientSite)
{
    Player *This = OLEOBJ_THIS(iface);
    HRESULT hrResult   = S_FALSE;
    IMoniker* pMoniker = NULL;
    LPOLESTR sDisplayName;
    int i;

    TRACE("%p\n", pClientSite);
    if (This->client)
        IOleClientSite_Release(This->client);
    This->client = pClientSite;
    if (This->client)
        IOleClientSite_AddRef(This->client);

    if(!pClientSite) return S_OK;
    hrResult = pClientSite->lpVtbl->GetMoniker(pClientSite, OLEGETMONIKER_TEMPFORUSER,
                                                   OLEWHICHMK_CONTAINER, &pMoniker);
    if(FAILED(hrResult)) return S_OK;
    hrResult = pMoniker->lpVtbl->GetDisplayName(pMoniker, NULL, NULL, &sDisplayName);
    pMoniker->lpVtbl->Release(pMoniker);
    if(FAILED(hrResult)) return S_OK;

    i = lstrlenW(sDisplayName);
    while (sDisplayName[i--] != '/' && i);
    sDisplayName[i+2] = 0;
    lstrcpyW(This->baseurl, sDisplayName);
    TRACE("Set baseurl -> %s\n", debugstr_w(This->baseurl));

    return S_OK;
}

static HRESULT WINAPI OleObject_GetClientSite(IOleObject *iface, IOleClientSite **ppClientSite)
{
    Player *This = OLEOBJ_THIS(iface);

    TRACE("%p -> %p\n", ppClientSite, This->client);

    if(!ppClientSite)
        return E_INVALIDARG;

    if(This->client)
        IOleClientSite_AddRef(This->client);
    *ppClientSite = This->client;

    return S_OK;
}

static HRESULT WINAPI OleObject_SetHostNames(IOleObject *iface, LPCOLESTR szContainerApp, LPCOLESTR szContainerObj)
{
    TRACE("is not implemented but returns S_OK \n");
    return S_OK;
}

static HRESULT WINAPI OleObject_Close(IOleObject *iface, DWORD dwSaveOption)
{
    FIXME("stub\n");
    return S_OK;
}

static HRESULT WINAPI OleObject_SetMoniker(IOleObject *iface, DWORD dwWhichMoniker, IMoniker *pmk)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleObject_GetMoniker(IOleObject *iface, DWORD dwAssign, DWORD dwWhichMoniker, IMoniker **ppmk)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleObject_InitFromData(IOleObject *iface, IDataObject *pDataObject, BOOL fCreation,
                                        DWORD dwReserved)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleObject_GetClipboardData(IOleObject *iface, DWORD dwReserved, IDataObject **ppDataObject)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleObject_DoVerb(IOleObject *iface, LONG iVerb, LPMSG lpmsg, IOleClientSite *pActiveSite,
                                        LONG lindex, HWND hwndParent, LPCRECT lprcPosRect)
{
    TRACE("call to %d\n", iVerb);

    if(iVerb != OLEIVERB_SHOW && iVerb != OLEIVERB_UIACTIVATE && iVerb != OLEIVERB_INPLACEACTIVATE)
    {
        TRACE("iVerb = %d not supported\n", iVerb);
        return E_NOTIMPL;
    }
    return S_OK;
}

static HRESULT WINAPI OleObject_EnumVerbs(IOleObject *iface, IEnumOLEVERB **ppEnumOleVerb)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleObject_Update(IOleObject *iface)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleObject_IsUpToDate(IOleObject *iface)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleObject_GetUserClassID(IOleObject *iface, CLSID *pClsid)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleObject_GetUserType(IOleObject *iface, DWORD dwFormOfType, LPOLESTR *pszUserType)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleObject_SetExtent(IOleObject *iface, DWORD dwDrawAspect, SIZEL *psizel)
{
    Player *This = OLEOBJ_THIS(iface);
    TRACE("%d %dx%d \n", dwDrawAspect, psizel->cx, psizel->cy);
    This->sizel.cx = psizel->cx;
    This->sizel.cy = psizel->cy;
    return S_OK;
}

static HRESULT WINAPI OleObject_GetExtent(IOleObject *iface, DWORD dwDrawAspect, SIZEL *psizel)
{
    Player *This = OLEOBJ_THIS(iface);
/*    if (!psizel) 
    {
        TRACE("alloc\n");
        psizel = HeapAlloc(GetProcessHeap(), 0, sizeof(SIZEL));
    }
*/
    psizel->cx = This->sizel.cx;
    psizel->cy = This->sizel.cy;
    TRACE("%d %d\n", psizel->cx, psizel->cy);
    return S_OK;
}

static HRESULT WINAPI OleObject_Advise(IOleObject *iface, IAdviseSink *pAdvSink, DWORD *pdwConnection)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleObject_Unadvise(IOleObject *iface, DWORD dwConnection)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleObject_EnumAdvise(IOleObject *iface, IEnumSTATDATA **ppenumAdvise)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleObject_GetMiscStatus(IOleObject *iface, DWORD dwAspect, DWORD *pdwStatus)
{
    TRACE("returns pdwStatus = 1\n");
    *pdwStatus = 1;
    return S_OK;
}

static HRESULT WINAPI OleObject_SetColorScheme(IOleObject *iface, LOGPALETTE *pLogpal)
{
    TRACE("is not implemented but returns S_OK \n");
    return S_OK;
}

const IOleObjectVtbl OleObjectVtbl = {
    OleObject_QueryInterface,
    OleObject_AddRef,
    OleObject_Release,
    OleObject_SetClientSite,
    OleObject_GetClientSite,
    OleObject_SetHostNames,
    OleObject_Close,
    OleObject_SetMoniker,
    OleObject_GetMoniker,
    OleObject_InitFromData,
    OleObject_GetClipboardData,
    OleObject_DoVerb,
    OleObject_EnumVerbs,
    OleObject_Update,
    OleObject_IsUpToDate,
    OleObject_GetUserClassID,
    OleObject_GetUserType,
    OleObject_SetExtent,
    OleObject_GetExtent,
    OleObject_Advise,
    OleObject_Unadvise,
    OleObject_EnumAdvise,
    OleObject_GetMiscStatus,
    OleObject_SetColorScheme
};

#undef OLEOBJ_THIS

/**********************************************************
 * IOleControl implementation
 */

#define CONTROL_THIS(iface) DEFINE_THIS(Player, OleControl, iface)

static HRESULT WINAPI OleControl_QueryInterface(IOleControl *iface, REFIID riid, void **ppvObject)
{
    Player *This = CONTROL_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI OleControl_AddRef(IOleControl *iface)
{
    Player *This = CONTROL_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI OleControl_Release(IOleControl *iface)
{
    Player *This = CONTROL_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

static HRESULT WINAPI OleControl_GetControlInfo(IOleControl *iface, CONTROLINFO *pCI)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleControl_OnMnemonic(IOleControl *iface, MSG *pMsg)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleControl_OnAmbientPropertyChange(IOleControl *iface, DISPID dispID)
{
    TRACE("DISPID = %d\n", dispID);
    return S_OK;
}

static HRESULT WINAPI OleControl_FreezeEvents(IOleControl *iface, BOOL bFreeze)
{
    FIXME("always returns TRUE\n");
    /* bFreeze = TRUE; */
    return S_OK;
}


static const IOleControlVtbl OleControlVtbl = {
    OleControl_QueryInterface,
    OleControl_AddRef,
    OleControl_Release,
    OleControl_GetControlInfo,
    OleControl_OnMnemonic,
    OleControl_OnAmbientPropertyChange,
    OleControl_FreezeEvents
};

#undef CONTROL_THIS


/**********************************************************
 * IOleCommandTarget implementation
 */

#define CMDTARGET_THIS(iface) DEFINE_THIS(Player, OleCommandTarget, iface)

static HRESULT WINAPI OleCommandTarget_QueryInterface(
             IOleCommandTarget* iface,
             REFIID riid,
             void **ppvObject)
{
    Player *This = CMDTARGET_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI OleCommandTarget_AddRef(IOleCommandTarget* iface)
{
    Player *This = CMDTARGET_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI OleCommandTarget_Release(IOleCommandTarget* iface)
{
    Player *This = CMDTARGET_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IOleCommandTarget methods ***/
static HRESULT WINAPI OleCommandTarget_QueryStatus(
             IOleCommandTarget* iface,
             const GUID *pguidCmdGroup,
             ULONG cCmds,
             OLECMD prgCmds[],
             OLECMDTEXT *pCmdText)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleCommandTarget_Exec(
             IOleCommandTarget* iface,
             const GUID *pguidCmdGroup,
             DWORD nCmdID,
             DWORD nCmdexecopt,
             VARIANT *pvaIn,
             VARIANT *pvaOut)
{
    WCHAR buf[39];
    if(!pguidCmdGroup)
    {
        if(nCmdID < OLECMDID_OPEN || nCmdID > OLECMDID_GETPRINTTEMPLATE /*|| !exec_table[nCmdID].func*/) {
            TRACE("Unsupported cmdID = %d\n", nCmdID);
            return OLECMDERR_E_NOTSUPPORTED;
        }
        switch(nCmdID)
        {
            case OLECMDID_STOP:
                FIXME("OLECMDID_STOP is not implemented yet\n");
                return S_OK;
                break;
            default:
                TRACE("cmdID = %d\n", nCmdID);
        }
        return S_OK;
    }
    StringFromGUID2(pguidCmdGroup, buf, 39);
    TRACE("pguidCmdGroup %s\n", debugstr_w(buf));
    return OLECMDERR_E_UNKNOWNGROUP;
}

static const IOleCommandTargetVtbl OleCommandTargetVtbl = {
    OleCommandTarget_QueryInterface,
    OleCommandTarget_AddRef,
    OleCommandTarget_Release,
    OleCommandTarget_QueryStatus,
    OleCommandTarget_Exec
};

#undef CMDTARGET_THIS

void Player_OleObj_Init(Player *This)
{
    This->lpOleObjectVtbl = &OleObjectVtbl;
    This->lpOleControlVtbl = &OleControlVtbl;
    This->lpOleCommandTargetVtbl = &OleCommandTargetVtbl;
}
