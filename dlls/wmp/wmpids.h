/*
 * Dispatch ID definitions for all WMP interfaces
 *
 * Copyright 2008 Vitaly Lipatov (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __WMPIDS_H__
#define __WMPIDS_H__

/* IWMPCore */
#define DISPID_WMPCORE_URL                  1
#define DISPID_WMPCORE_OPENSTATE            2
#define DISPID_WMPCORE_CLOSE                3
#define DISPID_WMPCORE_CONTROLS             4
#define DISPID_WMPCORE_SETTINGS             5
#define DISPID_WMPCORE_CURRENTMEDIA         6
#define DISPID_WMPCORE_NETWORK              7
#define DISPID_WMPCORE_MEDIACOLLECTION      8
#define DISPID_WMPCORE_PLAYLISTCOLLECTION   9
#define DISPID_WMPCORE_PLAYSTATE            10
#define DISPID_WMPCORE_VERSIONINFO          11
#define DISPID_WMPCORE_LAUNCHURL            12
#define DISPID_WMPCORE_CURRENTPLAYLIST      13
#define DISPID_WMPCORE_CDROMCOLLECTION      14
#define DISPID_WMPCORE_CLOSEDCAPTION        15
#define DISPID_WMPCORE_ISONLINE             16
#define DISPID_WMPCORE_ERROR                17
#define DISPID_WMPCORE_STATUS               18

/* IPlayer */
#define DISPID_WMPOCX_ENABLED               19
#define DISPID_WMPOCX_TRANSPARENTATSTART    20
#define DISPID_WMPOCX_FULLSCREEN            21
#define DISPID_WMPOCX_ENABLECONTEXTMENU     22
#define DISPID_WMPOCX_UIMODE                23


/* IWMPControl */
#define DISPID_WMPCONTROLS_PLAY                  51
#define DISPID_WMPCONTROLS_STOP                  52
#define DISPID_WMPCONTROLS_PAUSE                 53
#define DISPID_WMPCONTROLS_FASTFORWARD           54
#define DISPID_WMPCONTROLS_FASTREVERSE           55
#define DISPID_WMPCONTROLS_CURRENTPOSITION       56
#define DISPID_WMPCONTROLS_CURRENTPOSITIONSTRING 57
#define DISPID_WMPCONTROLS_NEXT                  58
#define DISPID_WMPCONTROLS_PREVIOUS              59
#define DISPID_WMPCONTROLS_CURRENTITEM           60
#define DISPID_WMPCONTROLS_CURRENTMARKER         61
#define DISPID_WMPCONTROLS_ISAVAILABLE           62
#define DISPID_WMPCONTROLS_PLAYITEM              63
#define DISPID_WMPCONTROLS2_STEP                 64
#define DISPID_WMPCONTROLS3_AUDIOLANGUAGECOUNT   65
#define DISPID_WMPCONTROLS3_GETAUDIOLANGUAGEID   66
#define DISPID_WMPCONTROLS3_GETAUDIOLANGUAGEDESC 67
#define DISPID_WMPCONTROLS3_CURRENTAUDIOLANGUAGE 68
#define DISPID_WMPCONTROLS3_CURRENTAUDIOLANGUAGEINDEX 69
#define DISPID_WMPCONTROLS3_GETLANGUAGENAME      70
#define DISPID_WMPCONTROLS3_CURRENTPOSITIONTIMECODE 71
#define DISPID_WMPCONTROLSFAKE_TIMECOMPRESSION   72

#endif /* __WMPIDS_H__ */
