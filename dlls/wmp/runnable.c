/*
 * Copyright 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdarg.h>
#include <stdio.h>
#include "eterwmp.h"

#define COBJMACROS

/**********************************************************
 * IRunnableObject methods implementation
 */

#define RUNOBJ_THIS(iface) DEFINE_THIS(Player, RunnableObject, iface)

static HRESULT WINAPI RunnableObject_QueryInterface(
        IRunnableObject* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = RUNOBJ_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI RunnableObject_AddRef(
        IRunnableObject* iface)
{
    Player *This = RUNOBJ_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI RunnableObject_Release(
        IRunnableObject* iface)
{
    Player *This = RUNOBJ_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IRunnableObject methods ***/
static HRESULT WINAPI RunnableObject_GetRunningClass(
        IRunnableObject* iface,
        LPCLSID lpClsid)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI RunnableObject_Run(
        IRunnableObject* iface,
        LPBINDCTX pbc)
{
    FIXME("pbc = %p \n", pbc);
    return S_OK;
}

static BOOL WINAPI RunnableObject_IsRunning(
        IRunnableObject* iface)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI RunnableObject_LockRunning(
        IRunnableObject* iface,
        BOOL fLock,
        BOOL fLastUnlockCloses)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI RunnableObject_SetContainedObject(
        IRunnableObject* iface,
        BOOL fContained)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}


static const IRunnableObjectVtbl RunnableObjectVtbl =
{
    RunnableObject_QueryInterface,
    RunnableObject_AddRef,
    RunnableObject_Release,
    RunnableObject_GetRunningClass,
    RunnableObject_Run,
    RunnableObject_IsRunning,
    RunnableObject_LockRunning,
    RunnableObject_SetContainedObject
};

#undef RUNOBJ_THIS

void Player_RunObj_Init(Player *This)
{
    This->lpRunnableObjectVtbl = &RunnableObjectVtbl;
}
