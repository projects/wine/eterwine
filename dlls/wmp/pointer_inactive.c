/*
 * Copyright 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"

/**********************************************************
 * IPointerInactive methods implementation
 */

#define INACTIVE_THIS(iface) DEFINE_THIS(Player, PointerInactive, iface)

static HRESULT WINAPI PointerInactive_QueryInterface(
        IPointerInactive* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = INACTIVE_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI PointerInactive_AddRef(
        IPointerInactive* iface)
{
    Player *This = INACTIVE_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI PointerInactive_Release(
        IPointerInactive* iface)
{
    Player *This = INACTIVE_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IPointerInactive methods ***/
static HRESULT WINAPI PointerInactive_GetActivationPolicy(
        IPointerInactive* iface,
        DWORD *pdwPolicy)
{
    TRACE("returns POINTERINACTIVE_ACTIVATEONENTRY \n");
    *pdwPolicy = 1;
    return S_OK;
}


static HRESULT WINAPI PointerInactive_OnInactiveMouseMove(
        IPointerInactive* iface,
        LPCRECT pRectBounds,
        LONG x,
        LONG y,
        DWORD grfKeyState)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}


static HRESULT WINAPI PointerInactive_OnInactiveSetCursor(
        IPointerInactive* iface,
        LPCRECT pRectBounds,
        LONG x,
        LONG y,
        DWORD dwMouseMsg,
        BOOL fSetAlways)
{
    TRACE("is not implemented \n");
    return E_NOTIMPL;
}

static const IPointerInactiveVtbl PointerInactiveVtbl =
{
    PointerInactive_QueryInterface,
    PointerInactive_AddRef,
    PointerInactive_Release,
    PointerInactive_GetActivationPolicy,
    PointerInactive_OnInactiveMouseMove,
    PointerInactive_OnInactiveSetCursor
};

#undef INACTIVE_THIS

void Player_PointerInactive_Init(Player *This)
{
    This->lpPointerInactiveVtbl = &PointerInactiveVtbl;
}
