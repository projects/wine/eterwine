/*
 * Register and unregister wmp.dll functions
 *
 * Copyright (C) 2002 John K. Hohm
 * Copyright (C) 2007 Roy Shea (Google)
 * Copyright (C) 2008 Sinitsin Ivan (Etersoft)
 * Copyright (C) 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"

static ULONG WINAPI WMP_IClassFactory_AddRef(LPCLASSFACTORY iface)
{
    ClassFactoryImpl *This = (ClassFactoryImpl *)iface;
    ULONG ref;

    TRACE("\n");

    if (This == NULL) return E_POINTER;

    ref = InterlockedIncrement(&This->ref);
    if (ref == 1) {
        InterlockedIncrement(&dll_ref);
    }
    return ref;
}

static HRESULT WINAPI WMP_IClassFactory_QueryInterface(
        LPCLASSFACTORY iface,
        REFIID riid,
        LPVOID *ppvObj)
{
    if(IsEqualGUID(&IID_IClassFactory, riid) || IsEqualGUID(&IID_IUnknown, riid)) {
        IClassFactory_AddRef(iface);
        *ppvObj = iface;
        return S_OK;
    }

    WARN("not supported iid %s\n", debugstr_guid(riid));
    *ppvObj = NULL;
    return E_NOINTERFACE;
}

static ULONG WINAPI WMP_IClassFactory_Release(LPCLASSFACTORY iface)
{
    ClassFactoryImpl *This = (ClassFactoryImpl *)iface;
    ULONG ref = InterlockedDecrement(&This->ref);

    TRACE("(%p) ref = %u\n", This, ref);

    if (ref == 0) {
        HeapFree(GetProcessHeap(), 0, This);
        InterlockedDecrement(&dll_ref);
    }
    return ref;
}

static HRESULT WINAPI WMP_IClassFactory_CreateInstance(
        LPCLASSFACTORY iface,
        LPUNKNOWN pUnkOuter,
        REFIID riid,
        LPVOID *ppvObj)
{
    return Player_Create(pUnkOuter, riid, ppvObj);
}

static HRESULT WINAPI WMP_IClassFactory_LockServer(
        LPCLASSFACTORY iface,
        BOOL fLock)
{
    TRACE("\n");

    if (fLock != FALSE) {
        IClassFactory_AddRef(iface);
    } else {
        IClassFactory_Release(iface);
    }
    return S_OK;
}

static const IClassFactoryVtbl ClassFactoryVtbl =
{
    WMP_IClassFactory_QueryInterface,
    WMP_IClassFactory_AddRef,
    WMP_IClassFactory_Release,
    WMP_IClassFactory_CreateInstance,
    WMP_IClassFactory_LockServer
};

ClassFactoryImpl classfactory =
{
    &ClassFactoryVtbl,
    0
};
