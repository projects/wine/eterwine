/*
 * Copyright 2008 Konstantin Kondratyuk
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdarg.h>
#include <stdio.h>

#define COBJMACROS

#include "windef.h"
#include "winbase.h"
#include "ole2.h"
#include "shlguid.h"
#include "eterwmp.h"
#include "wmp.h"


static HRESULT WINAPI OleClientSite_QueryInterface(IOleClientSite *iface, REFIID riid, void **ppv)
{
    TRACE("wmp.dll: oleclientsite.c: OleClientSite_QueryInterface is not implemented \n");
    return E_NOTIMPL;
}
static ULONG WINAPI OleClientSite_AddRef(IOleClientSite *iface)
{
    TRACE("wmp.dll: oleclientsite.c: OleClientSite_AddRef is not implemented \n");
    return E_NOTIMPL;
}
static ULONG WINAPI OleClientSite_Release(IOleClientSite *iface)
{
    TRACE("wmp.dll: oleclientsite.c: OleClientSite_Release is not implemented \n");
    return E_NOTIMPL;
}
static HRESULT WINAPI OleClientSite_SaveObject(IOleClientSite *iface)
{
    TRACE("wmp.dll: oleclientsite.c: OleClientSite_SaveObject is not implemented \n");
    return E_NOTIMPL;
}
static HRESULT WINAPI OleClientSite_GetMoniker(IOleClientSite *iface, DWORD dwAssign, DWORD dwWhichMoniker, IMoniker **ppmk)
{
    TRACE("wmp.dll: oleclientsite.c: OleClientSite_GetMoniker is not implemented \n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleClientSite_GetContainer(IOleClientSite *iface, IOleContainer **ppContainer)
{
    TRACE("wmp.dll: oleclientsite.c: OleClientSite_GetContainer is not implemented \n");
    return E_NOTIMPL;
}
static HRESULT WINAPI OleClientSite_ShowObject(IOleClientSite *iface)
{
    TRACE("wmp.dll: oleclientsite.c: OleClientSite_ShowObject is not implemented \n");
    return E_NOTIMPL;
}
static HRESULT WINAPI OleClientSite_OnShowWindow(IOleClientSite *iface, BOOL fShow)
{
    TRACE("wmp.dll: oleclientsite.c: OleClientSite_OnShowWindow is not implemented \n");
    return E_NOTIMPL;
}
static HRESULT WINAPI OleClientSite_RequestNewObjectLayout(IOleClientSite *iface)
{
    TRACE("wmp.dll: oleclientsite.c: OleClientSite_RequestNewObjectLayout is not implemented \n");
    return E_NOTIMPL;
}

static const IOleClientSiteVtbl OleClientSiteVtbl = {
    OleClientSite_QueryInterface,
    OleClientSite_AddRef,
    OleClientSite_Release,
    OleClientSite_SaveObject,
    OleClientSite_GetMoniker,
    OleClientSite_GetContainer,
    OleClientSite_ShowObject,
    OleClientSite_OnShowWindow,
    OleClientSite_RequestNewObjectLayout
};

/*
OleClientSiteImpl OleClientSite =
{
    &OleClientSiteVtbl,
    0
};
*/
