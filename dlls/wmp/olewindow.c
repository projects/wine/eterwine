/*
 * IOleInPlaceObject interface functions
 *
 * Copyright (C) 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"

/**********************************************************
 * IOleWindow methods implementation
 */

#define OLEWINDOW_THIS(iface) DEFINE_THIS(Player, OleWindow, iface)

static HRESULT WINAPI OleWindow_QueryInterface(
             IOleWindow* iface,
             REFIID riid,
             void **ppvObject)
{
    Player *This = OLEWINDOW_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI OleWindow_AddRef(
           IOleWindow* iface)
{
    Player *This = OLEWINDOW_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI OleWindow_Release(
           IOleWindow* iface)
{
    Player *This = OLEWINDOW_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IOleWindow methods ***/
static HRESULT WINAPI OleWindow_GetWindow(
             IOleWindow* iface,
             HWND *phwnd)
{
    FIXME("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleWindow_ContextSensitiveHelp(
             IOleWindow* iface,
             BOOL fEnterMode)
{
    FIXME("is not implemented\n");
    return E_NOTIMPL;
}

const IOleWindowVtbl OleWindowVtbl =
{
    OleWindow_QueryInterface,
    OleWindow_AddRef,
    OleWindow_Release,
    OleWindow_GetWindow,
    OleWindow_ContextSensitiveHelp
};

#undef OLEWINDOW_THIS


/**********************************************************
 * IOleInPlaceObject methods implementation
 */

#define INPLACE_THIS(iface) DEFINE_THIS(Player, OleInPlaceObject, iface)

static HRESULT WINAPI OleInPlaceObject_QueryInterface(
        IOleInPlaceObject* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = INPLACE_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI OleInPlaceObject_AddRef(
        IOleInPlaceObject* iface)
{
    Player *This = INPLACE_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI OleInPlaceObject_Release(
        IOleInPlaceObject* iface)
{
    Player *This = INPLACE_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IOleWindow methods ***/
static HRESULT WINAPI OleInPlaceObject_GetWindow(
        IOleInPlaceObject* iface,
        HWND *phwnd)
{
    TRACE("\n");
    return OleWindow_GetWindow((IOleWindow*) iface, phwnd);
}

static HRESULT WINAPI OleInPlaceObject_ContextSensitiveHelp(
        IOleInPlaceObject* iface,
        BOOL fEnterMode)
{
    TRACE("\n");
    return OleWindow_ContextSensitiveHelp((IOleWindow*) iface, fEnterMode);
}

    /*** IOleInPlaceObject methods ***/
static HRESULT WINAPI OleInPlaceObject_InPlaceDeactivate(
        IOleInPlaceObject* iface)
{
    FIXME("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleInPlaceObject_UIDeactivate(
        IOleInPlaceObject* iface)
{
    FIXME("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleInPlaceObject_SetObjectRects(
        IOleInPlaceObject* iface,
        LPCRECT lprcPosRect,
        LPCRECT lprcClipRect)
{
    FIXME("(%d %d %d %d) - (%d %d %d %d)\n",
          lprcPosRect->left, lprcPosRect->top, lprcPosRect->right, lprcPosRect->bottom,
          lprcClipRect->left, lprcClipRect->top, lprcClipRect->right, lprcClipRect->bottom);
    return S_OK;
}

static HRESULT WINAPI OleInPlaceObject_ReactivateAndUndo(
        IOleInPlaceObject* iface)
{
    FIXME("is not implemented\n");
    return E_NOTIMPL;
}

const IOleInPlaceObjectVtbl OleInPlaceObjectVtbl =
{
    OleInPlaceObject_QueryInterface,
    OleInPlaceObject_AddRef,
    OleInPlaceObject_Release,
    OleInPlaceObject_GetWindow,
    OleInPlaceObject_ContextSensitiveHelp,
    OleInPlaceObject_InPlaceDeactivate,
    OleInPlaceObject_UIDeactivate,
    OleInPlaceObject_SetObjectRects,
    OleInPlaceObject_ReactivateAndUndo
};

#undef INPLACE_THIS


/**********************************************************
 * IOleInPlaceObjectWindowless methods implementation
 */

#define INPLACEW_THIS(iface) DEFINE_THIS(Player, OleInPlaceObjectWindowless, iface)

static HRESULT WINAPI OleInPlaceObjectWindowless_QueryInterface(
        IOleInPlaceObjectWindowless* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = INPLACEW_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI OleInPlaceObjectWindowless_AddRef(
        IOleInPlaceObjectWindowless* iface)
{
    Player *This = INPLACEW_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI OleInPlaceObjectWindowless_Release(
        IOleInPlaceObjectWindowless* iface)
{
    Player *This = INPLACEW_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IOleWindow methods ***/
static HRESULT WINAPI OleInPlaceObjectWindowless_GetWindow(
        IOleInPlaceObjectWindowless* iface,
        HWND *phwnd)
{
    TRACE("\n");
    return OleWindow_GetWindow((IOleWindow*) iface, phwnd);
}

static HRESULT WINAPI OleInPlaceObjectWindowless_ContextSensitiveHelp(
        IOleInPlaceObjectWindowless* iface,
        BOOL fEnterMode)
{
    TRACE("\n");
    return OleWindow_ContextSensitiveHelp((IOleWindow*) iface, fEnterMode);
}

    /*** IOleInPlaceObject methods ***/
    static HRESULT WINAPI OleInPlaceObjectWindowless_InPlaceDeactivate(
            IOleInPlaceObjectWindowless* iface)
{
    TRACE("\n");
    return OleInPlaceObject_InPlaceDeactivate((IOleInPlaceObject*) iface);
}

static HRESULT WINAPI OleInPlaceObjectWindowless_UIDeactivate(
        IOleInPlaceObjectWindowless* iface)
{
    TRACE("\n");
    return OleInPlaceObject_UIDeactivate((IOleInPlaceObject*) iface);
}

static HRESULT WINAPI OleInPlaceObjectWindowless_SetObjectRects(
        IOleInPlaceObjectWindowless* iface,
        LPCRECT lprcPosRect,
        LPCRECT lprcClipRect)
{
    TRACE("\n");
    return OleInPlaceObject_SetObjectRects((IOleInPlaceObject*) iface, lprcPosRect, lprcClipRect);
}

static HRESULT WINAPI OleInPlaceObjectWindowless_ReactivateAndUndo(
        IOleInPlaceObjectWindowless* iface)
{
    TRACE("\n");
    return OleInPlaceObject_ReactivateAndUndo((IOleInPlaceObject*) iface);
}
    /*** IOleInPlaceObjectWindowless methods ***/
static HRESULT WINAPI OleInPlaceObjectWindowless_OnWindowMessage(
             IOleInPlaceObjectWindowless* iface,
             UINT msg,
             WPARAM wParam,
             LPARAM lParam,
             LRESULT *plResult)
{
    FIXME("is not implemented\n");
    return E_NOTIMPL;
}

static HRESULT WINAPI OleInPlaceObjectWindowless_GetDropTarget(
             IOleInPlaceObjectWindowless* iface,
             IDropTarget **ppDropTarget)
{
    FIXME("is not implemented\n");
    return E_NOTIMPL;
}

    const IOleInPlaceObjectWindowlessVtbl OleInPlaceObjectWindowlessVtbl =
{
    OleInPlaceObjectWindowless_QueryInterface,
    OleInPlaceObjectWindowless_AddRef,
    OleInPlaceObjectWindowless_Release,
    OleInPlaceObjectWindowless_GetWindow,
    OleInPlaceObjectWindowless_ContextSensitiveHelp,
    OleInPlaceObjectWindowless_InPlaceDeactivate,
    OleInPlaceObjectWindowless_UIDeactivate,
    OleInPlaceObjectWindowless_SetObjectRects,
    OleInPlaceObjectWindowless_ReactivateAndUndo,
    OleInPlaceObjectWindowless_OnWindowMessage,
    OleInPlaceObjectWindowless_GetDropTarget
};

#undef INPLACEW_THIS

void Player_OleWindow_Init(Player *This)
{
    This->lpOleWindowVtbl = &OleWindowVtbl;
    This->lpOleInPlaceObjectVtbl = &OleInPlaceObjectVtbl;
    This->lpOleInPlaceObjectWindowlessVtbl = &OleInPlaceObjectWindowlessVtbl;
}
