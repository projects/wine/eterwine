/*
 * Copyright 2008 Konstantin Kondratyuk (Etersoft)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "eterwmp.h"

/**********************************************************
 * IObjectSafety methods implementation
 */

#define SAFETY_THIS(iface) DEFINE_THIS(Player, ObjectSafety, iface)

static HRESULT WINAPI ObjectSafety_QueryInterface(
        IObjectSafety* iface,
        REFIID riid,
        void **ppvObject)
{
    Player *This = SAFETY_THIS(iface);
    return IWMPCore_QueryInterface(CORE(This), riid, ppvObject);
}

static ULONG WINAPI ObjectSafety_AddRef(
        IObjectSafety* iface)
{
    Player *This = SAFETY_THIS(iface);
    ULONG ref = InterlockedIncrement(&This->ref);
    TRACE("(%p) ref = %u\n", This, ref);
    return ref;
}

static ULONG WINAPI ObjectSafety_Release(
        IObjectSafety* iface)
{
    Player *This = SAFETY_THIS(iface);
    return IWMPCore_Release(CORE(This));
}

    /*** IObjectSafety methods ***/
static HRESULT WINAPI ObjectSafety_GetInterfaceSafetyOptions(
        IObjectSafety* iface,
        REFIID riid,
        DWORD *pdwSupportedOptions,
        DWORD *pdwEnabledOptions)
{
    FIXME("returns INTERFACE_USES_DISPEX\n");
    *pdwSupportedOptions = INTERFACE_USES_DISPEX;
    *pdwEnabledOptions = INTERFACE_USES_DISPEX;
    return S_OK;
}

static HRESULT WINAPI ObjectSafety_SetInterfaceSafetyOptions(
        IObjectSafety* iface,
        REFIID riid,
        DWORD dwOptionSetMask,
        DWORD dwEnabledOptions)
{
    FIXME("is not implemented  but returns S_OK\n");
    return S_OK;
}


static const IObjectSafetyVtbl ObjectSafetyVtbl =
{
    ObjectSafety_QueryInterface,
    ObjectSafety_AddRef,
    ObjectSafety_Release,
    ObjectSafety_GetInterfaceSafetyOptions,
    ObjectSafety_SetInterfaceSafetyOptions
};

#undef SAFETY_THIS

void Player_Safety_Init(Player *This)
{
    This->lpObjectSafetyVtbl = &ObjectSafetyVtbl;
}
